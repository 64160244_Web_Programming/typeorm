import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"


AppDataSource.initialize().then(async () => {

    console.log("Inserting a new product into the database...")
    const productsReposity = AppDataSource.getRepository(Product)
    

    const products = await productsReposity.find()
    console.log("Loaded products: ", products)

    const updatedProduct = await productsReposity.findOneBy({id:1})
    console.log(updatedProduct)
    updatedProduct.price=80
    await productsReposity.save(updatedProduct)
}).catch(error => console.log(error))
